﻿using System.Threading.Tasks;

namespace HMACAuthentication
{
    public interface ISecretLookup
    {
        Task<string> LookupSecretAsync(string apiKey);
        Task<byte[]> LookupSecretBytesAsync(string apiKey);
    }
}
