﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HMACAuthentication
{
    public sealed class HeaderPayload
    {
        public string APIKey { get; set; }
        public string Nonce { get; set; }
        public string Signature { get; set; }
        public long UnixUTCTimeSeconds { get; set; }
    }
}