﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HMACAuthentication
{
    public interface IRolesLookup
    {
        Task<string[]> LookupRolesAsync(string apiKey);
    }
}
