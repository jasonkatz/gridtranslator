﻿using HMACAuthentication.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Principal;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace HMACAuthentication
{
    public sealed class HMACAuthenticationHandler : AuthenticationHandler<HMACAuthenticationOptions>
    {
        private const string AuthorizationHeader = "Authorization";
        private readonly ISecretLookup _secretLookup;
        private readonly IRolesLookup _rolesLookup;

        public HMACAuthenticationHandler(IOptionsMonitor<HMACAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            ISecretLookup secretLookup,
            IRolesLookup rolesLookup) : base(options, logger, encoder, clock)
        {
            this._secretLookup = secretLookup;
            this._rolesLookup = rolesLookup;
        }

        protected async override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var header = SplitAuthenticationHeader();
            if (header == null)
                return AuthenticateResult.NoResult();

            //check time drift
            var currentUTCUnixTimestamp = Clock.UtcNow.ToUnixTimeSeconds();
            Logger.LogDebug($"currentUTCUnixTimestamp: {currentUTCUnixTimestamp}");
            Logger.LogDebug($"header.UnixUTCTimeSeconds: {header.UnixUTCTimeSeconds}");
            Logger.LogDebug($"Difference: {Math.Abs(currentUTCUnixTimestamp - header.UnixUTCTimeSeconds)}");
            if (Math.Abs(currentUTCUnixTimestamp - header.UnixUTCTimeSeconds) > Options.AllowedTimestampDriftSeconds)
                return AuthenticateResult.Fail("Date drift exceeded max");

            //check secret
            Logger.LogDebug($"Looking up secret for {header.APIKey}");
            var secret = await _secretLookup.LookupSecretAsync(header.APIKey);

            if (secret == null)
            {
                Logger.LogWarning($"No secret found for {header.APIKey}");
                return AuthenticateResult.Fail("Invalid APIKey");
            }
            else if (secret.Length != 32)
            {
                Logger.LogWarning($"Invalid secret size. Secret must be 32 bytes.");
                throw new InvalidOperationException("Invalid secret size");
            }

            //check signature
            HttpRequestRewindExtensions.EnableBuffering(Request);
            var body = "multipart/form-data";
            if (!Request.Headers.ContainsKey("Content-Type") || !Request.Headers["Content-Type"].ToString().ToLower().Contains("multipart/form-data"))
            {
                using (StreamReader sr = new StreamReader(Request.Body, Encoding.UTF8, detectEncodingFromByteOrderMarks: false, leaveOpen: true))
                {
                    body = await sr.ReadToEndAsync();
                    Request.Body.Position = 0;
                }
            }
            var uri = Microsoft.AspNetCore.Http.Extensions.UriHelper.GetEncodedUrl(Request).Replace("https://", "").Replace("http://", "").ToLower();
            Logger.LogDebug($"APIKey: {header.APIKey}");
            Logger.LogDebug($"Secret: {secret.Substring(0, 5)}...");
            Logger.LogDebug($"Request.Method: {Request.Method}");
            Logger.LogDebug($"Request.Path: {uri}");
            Logger.LogDebug($"Nonce: {header.Nonce}");
            Logger.LogDebug($"UnixUTCTimeSeconds: {header.UnixUTCTimeSeconds}");
            var signature = SignatureHelper.GenerateSignature(secret, Request.Method, uri, body, header.APIKey, header.Nonce, header.UnixUTCTimeSeconds);
            Logger.LogDebug($"Calculated server side HMAC: {signature}");
            if (signature == header.Signature)
            {
                return AuthenticateResult.Success(new AuthenticationTicket(
                    new GenericPrincipal(new GenericIdentity(header.APIKey), await _rolesLookup.LookupRolesAsync(header.APIKey)),
                    new AuthenticationProperties() { IsPersistent = false, AllowRefresh = false },
                    Options.Schema));
            }
            else
            {
                return AuthenticateResult.Fail("Invalid signature");
            }
        }

        private HeaderPayload SplitAuthenticationHeader()
        {
            var header = Request.Headers[AuthorizationHeader].SingleOrDefault();
            if (header == null)
            {
                Logger.LogDebug("AuthorizationHeader not found");
                return null;
            }

            var split = header.Split(' ', ':');
            if (split.Length != 5)
            {
                Logger.LogInformation($"AuthorizationHeader split into {split.Length} args...");
                return null;
            }

            if (split[0] != "HMAC-SHA256")
            {
                Logger.LogInformation($"AuthorizationHeader does not start with HMAC-SHA256");
                return null;
            }

            if (!long.TryParse(split[4], out long unixTS))
            {
                Logger.LogInformation("Unable to parse unix timestamp");
                return null;
            }

            return new HeaderPayload
            {
                APIKey = split[1],
                Nonce = split[2],
                Signature = split[3],
                UnixUTCTimeSeconds = unixTS
            };
        }
    }
}
