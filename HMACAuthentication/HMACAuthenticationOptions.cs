﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace HMACAuthentication
{
    public class HMACAuthenticationOptions : AuthenticationSchemeOptions
    {
        public const string DefaultSchema = "HMAC-SHA256";
        public string Schema => DefaultSchema;
        public int AllowedTimestampDriftSeconds { get; set; } = 5 * 60;
        //public Func<string, string[]> GetRolesForId { get; set; }
    }
}
