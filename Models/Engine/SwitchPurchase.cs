﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class SwitchPurchase
    {
        public string Ticker { get; set; }
        public decimal Percentage { get; set; }
        public string CorrelationID { get; set; }
        public string InternalID { get; set; }
    }
}
