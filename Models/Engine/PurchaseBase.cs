﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class PurchaseBase
    {
        public string Ticker { get; set; }
        public decimal CashOrderQuantity { get; set; }
        public RecurringInfo Recurring { get; set; } //Required only for recurring instructions
        public string CorrelationID { get; set; }
        public string InternalID { get; set; } // must be unique per client
    }
}
