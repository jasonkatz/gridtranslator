﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class InstructionListBase
    {
        public string ListID { get; set; } //required?
        public string RegistrationID { get; set; } //new business only
        public string AdvisorCode { get; set; }
        public string InvestmentCode { get; set; } //Required for existing account only
        public string BankAccountCode { get; set; }
    }
}
