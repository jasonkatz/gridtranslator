﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class RecurringInfo
    {
        public FrequencyType FrequencyType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PreferredDay { get; set; }
        public EscalationInfo Escalation { get; set; } //required only for escalating instructions
    }
}
