﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class SwitchInstruction
    {
        public string InvestmentCode { get; set; }
        public string AdvisorCode { get; set; }
        public string RedeemTicker { get; set; }
        public decimal RedeemAmount { get; set; }
        public RedeemType RedeemType { get; set; }
        public string CorrelationID { get; set; }
        public string RedeemInternalID { get; set; }
        public string ListID { get; set; } //required?
        public List<SwitchPurchase> Purchases { get; set; }
    }
}
