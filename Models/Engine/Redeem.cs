﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class Redeem : RedeemBase
    {
        public string InvestmentCode { get; set; }
        public string BankAccountCode { get; set; }
        public string AdvisorCode { get; set; } //is this still necessary? Not sure if it was Iress or Graphite?
    }
}
