﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class LinkedIndividual
    {
        public LinkedIndividualType LinkedIndividualType { get; set; }
        public Individual Individual { get; set; }
    }
}
