﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum AccountType
    {
        UNKNOWN = 0,
        CURRENT = 1,
        SAVINGS = 2
    }
}
