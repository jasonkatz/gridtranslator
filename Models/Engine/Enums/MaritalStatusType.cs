﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum MaritalStatusType
    {
        NOTSET = 0,
        UNKNOWN = 1,
        SINGLE = 2,
        MARRIEDINCOMMUNITYPROPERTY = 3,
        DIVORCED = 5,
        MARRIEDANTENUPTUAL = 6,
    }
}
