﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum LinkedIndividualType
    {
        TRUSTEE = 4,
        BENEFICIARY = 13,
        DIRECTOR = 5
    }
}
