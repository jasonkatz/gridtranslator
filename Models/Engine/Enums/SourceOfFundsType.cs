﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum SourceOfFundsType
    {
        UNKNOWN = 0,
        INVESTMENTSPOLICY = 1,
        BONUS = 2,
        CAPITALSAVINGS = 3,
        INHERITANCE = 4,
        SALARY = 5,
        SALEOFPROPERTY = 6,
        COMPANYPROFITS = 7,
        DONATIONGIFT = 8,
        CONTRIBUTIONS = 9,
        DIVIDENDREDEMPTION = 10,
        OTHER = 11,
        FUNDEDLIVINGANNUITY = 12,
        GUARANTEEDANNUITY = 13,
        INVESTMENTLINKEDLIVINGANNUITY = 14,
        PENSIONFUND = 15,
        PROVIDENTFUND = 16,
        PENSIONPRESERVATIONFUND = 17,
        RETIREMENTANNUITYFUND = 19
    }
}
