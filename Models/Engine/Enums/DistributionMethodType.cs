﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum DistributionMethodType
    {
        UNKNOWN = 0,
        REINVEST = 1,
        PAYOUT = 3
    }
}
