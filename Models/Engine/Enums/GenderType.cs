﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum GenderType
    {
        UNKNOWN = 0,
        MALE = 1,
        FEMALE = 2
    }
}
