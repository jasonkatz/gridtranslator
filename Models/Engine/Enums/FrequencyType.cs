﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum FrequencyType
    {
        UNKNOWN = 0,
        MONTHLY = 1,
        QUARTERLY = 2,
        BIANNUAL = 3,
        ANNUAL = 4
    }
}
