﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum TitleType
    {
        UNKNOWN = 0,
        MR = 1,
        MRS = 2,
        MS = 3,
        DR = 4,
        PROF = 5,
        Miss = 6,
        Master = 7,
        Drs = 8,
        The = 9,
        Adv = 10,
        Judge = 11,
        Rev = 12,
        MrAndMrs = 13,
        Minor = 14,
        EstateLate = 15
    }
}
