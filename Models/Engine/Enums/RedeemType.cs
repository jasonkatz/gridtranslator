﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum RedeemType
    {
        UNKNOWN = 0,
        CASHAMOUNT = 1,
        UNITS = 2,
        PERCENTAGE = 3
    }
}
