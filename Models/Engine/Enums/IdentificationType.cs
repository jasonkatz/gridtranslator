﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public enum IdentificationType
    {
        UNKNOWN = 0,
        IDENTITYDOCUMENT = 1,
        PASSPORT = 2
    }
}
