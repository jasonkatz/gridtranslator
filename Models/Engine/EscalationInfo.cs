﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class EscalationInfo
    {
        public decimal Percentage { get; set; }
        public FrequencyType FrequencyType { get; set; }
        public DateTime Date { get; set; }
    }
}
