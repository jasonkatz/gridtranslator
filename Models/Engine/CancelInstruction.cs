﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class CancelInstruction
    {
        public string CorrelationID { get; set; }
        public string OriginalCorrelationID { get; set; }
        public string TransactionID { get; set; }
        public string Ticker { get; set; }
        public InstructionType InstructionType { get; set; }
    }
}
