﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class RedeemBase
    {
        public string Ticker { get; set; }
        public decimal Amount { get; set; }
        public RedeemType RedeemType { get; set; }
        public RecurringInfo Recurring { get; set; } //only required for recurring instruction
        public string CorrelationID { get; set; }
        public string InternalID { get; set; }
    }
}
