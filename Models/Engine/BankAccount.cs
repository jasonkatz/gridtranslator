﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class BankAccount
    {
        public string AccountNumber { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public AccountType AccountType { get; set; }
        public string AccountHolder { get; set; }
        public string BankCode { get; set; }
        public string BankAccountCode { get; set; }
    }
}