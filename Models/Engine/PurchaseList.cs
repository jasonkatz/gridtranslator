﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class PurchaseList : InstructionListBase
    {
        public string InvestorCode { get; set; } //Required for new account
        public string ProductCode { get; set; } //Required for new investor or account
        public SourceOfFundsType SourceOfFundsType { get; set; }
        public string FeeCode { get; set; } //Required for new account
        public string FeePercentage { get; set; }
        public List<PurchaseBase> Purchases { get; set; }
        public bool EFT { get; set; }
        public LivingAnnuity LivingAnnuity { get; set; }
        public LivingAnnuityTransferTypeEnum? LivingAnnuityTransferType { get; set; }
        public string LATransferringFundName { get; set; }
        public string LATransferringFundEmail { get; set; }
    }
}
