﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class Entity
    {
        public EntityType EntityType { get; set; }
        public string InternalID { get; set; }
        public string RegistrationID { get; set; }
        public DistributionMethodType DistributionMethodType { get; set; }
        public string Email { get; set; }
        public string HomeNumber { get; set; }
        public string WorkNumber { get; set; }
        public string CellNumber { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public List<BankAccount> BankAccounts { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime InceptionDate { get; set; }
        public string VatNumber { get; set; }
        public DateTime VatRegistrationDate { get; set; }
        public string InvestorCode { get; set; }
        public string SvsStageID { get; set; }
        public List<LinkedIndividual> LinkedIndividuals { get; set; } //directors, trustees, etc
        public string PreviousRegistrationID { get; set; } //Is this necessary?
        public string CountryOfResidence { get; set; }
    }
}
