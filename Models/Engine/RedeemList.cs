﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class RedeemList : InstructionListBase
    {
        public List<RedeemBase> Redemptions { get; set; }
    }
}
