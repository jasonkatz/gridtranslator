using System;
using System.Collections.Generic;

namespace Models.Engine
{
    public class LivingAnnuity
    {
        public decimal IncomeAmount { get; set; }
        public AmountType AmountType { get; set; }
        public DateTime AnniversaryDate { get; set; }
        public decimal AnniversaryBalance { get; set; }
        public LivingAnnuityDirectiveDetail DirectiveDetail { get; set; }
        public List<DisinvestAsPerInstructionDetail> DisinvestAsPerInstruction { get; set; }
        public FrequencyType IncomeFrequency { get; set; }
        public bool SplitProportionally { get; set; }
        public DateTime LastPaymentDate { get; set; }
    }
    
    public enum AmountType
    {
        UNKNOWN = 0,
        CASHAMOUNT = 1,
        UNITS = 2,
        PERCENTAGE = 3
    }

    public class LivingAnnuityDirectiveDetail
    {
        public List<LivingAnnuityClientDirective> ClientDirectives { get; set; }
    }

    public class LivingAnnuityClientDirective
    {
        public decimal Amount { get; set; }
        public LivingAnnuityClientDirectiveType? DirectiveType { get; set; }
        public DateTime EndDate { get; set; }
        public int Percentage { get; set; }
        public DateTime StartDate { get; set; }
    }
    
    public enum LivingAnnuityClientDirectiveType
    {
        AA88 = 1,
        TAXRATEORPAYEE = 2
    }

    public class DisinvestAsPerInstructionDetail
    {
        public double Amount { get; set; }
        public double IncomeSplit { get; set; }
        public string Ticker { get; set; }
        public AmountType AmountType { get; set; }
    }
}