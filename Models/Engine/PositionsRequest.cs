﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class PositionsRequest
    {
        public string AdvisorCode { get; set; }
        public string InvestorCode { get; set; }
        public string InvestmentCode { get; set; }
        public string Ticker { get; set; }
    }
}
