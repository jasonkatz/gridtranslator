﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class Purchase : PurchaseBase
    {
        public string RegistrationID { get; set; } //Required for new investor only
        public string InvestorCode { get; set; } //Required for new account only
        public string InvestmentCode { get; set; } //Required for existing account only
        public string ProductCode { get; set; } //Required for new investor or account
        public string BankAccountCode { get; set; } //Required for new and existing account
        public string AdvisorCode { get; set; } //is this still necessary? Not sure if it was Iress or Graphite?
        public SourceOfFundsType SourceOfFundsType { get; set; }
        public string FeeCode { get; set; }
        public string FeePercentage { get; set; }
        public bool EFT { get; set; }
        public bool DummyInstruction { get; set; }
        public LivingAnnuity LivingAnnuity { get; set; }
        public LivingAnnuityTransferTypeEnum? LivingAnnuityTransferType { get; set; }
        public string LATransferringFundName { get; set; }
        public string LATransferringFundEmail { get; set; }
    }
}
