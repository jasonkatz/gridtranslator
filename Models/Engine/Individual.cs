﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Engine
{
    public class Individual
    {
        public string InternalID { get; set; }
        public string RegistrationID { get; set; }
        public DistributionMethodType DistributionMethodType { get; set; }
        public TitleType TitleType { get; set; }
        public string Surname { get; set; }
        public string FirstNames { get; set; }
        public string Initials { get; set; }
        public GenderType Gender { get; set; }
        public string Language { get; set; }
        public string Email { get; set; }
        public string HomeNumber { get; set; }
        public string WorkNumber { get; set; }
        public string CellNumber { get; set; }
        public MaritalStatusType MaritalStatusType { get; set; }
        public DateTime BirthDate { get; set; }
        public IdentificationType IdentificationType { get; set; }
        public string IDNumber { get; set; }
        public string PassportNumber { get; set; }
        public string CountryOfIssue { get; set; }
        public DateTime PassportExpiryDate { get; set; }
        public string TaxOffice { get; set; }
        public string TaxNumber { get; set; }
        public string CountryOfResidence { get; set; }
        public string Nationality { get; set; }
        public bool Resident { get; set; }
        public string CountryOfBirth { get; set; }
        public List<BankAccount> BankAccounts { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string InvestorCode { get; set; }
        public string SvsStageID { get; set; }
        public string PreviousRegistrationID { get; set; } //Is this necessary?
    }
}
