﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class PositionsReport
    {
        public string PositionsRequestID { get; set; } //necessary?
        public string MainPositionsRequestID { get; set; } //necessary?
        public string InvestmentCode { get; set; }
        public string InvestorCode { get; set; }
        public DateTime Date { get; set; }
        public string Ticker { get; set; }
        public decimal UnitsCleared { get; set; }
        public decimal UnitsUncleared { get; set; }
        public decimal UnitsCertified { get; set; }
        public decimal MarketValue { get; set; }
        public string HoldingCode { get; set; }
    }
}
