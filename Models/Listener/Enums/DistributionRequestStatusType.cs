﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum DistributionRequestStatusType
    {
        ACCEPTED = 0,
        COMPLETED = 1,
        REJECTED = 2
    }
}
