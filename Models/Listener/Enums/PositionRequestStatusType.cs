﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum PositionRequestStatusType
    {
        COMPLETED = 0,
        REJECTED = 2
    }
}
