﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum RegistrationResponseType
    {
        ACCEPTED = 0,
        HELD = 1,
        REJECTED = 2
    }
}
