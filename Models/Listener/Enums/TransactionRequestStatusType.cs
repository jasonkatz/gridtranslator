﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum TransactionRequestStatusType
    {
        ACCEPTED = 0,
        COMPLETED = 1,
        REJECTED = 2
    }
}
