﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum ExecutionResponseType
    {
        NEW = 0,
        PARTIALLYFILLED = 1,
        FILLED = 2,
        CANCELLED = 4,
        PENDINGCANCEL = 6,
        REJECTED = 8
    }
}
