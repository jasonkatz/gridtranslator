﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public enum PositionRequestResultType
    {
        VALID = 0,
        INVALID = 1,
        NOPOSITIONS = 2,
        OTHER = 99
    }
}
