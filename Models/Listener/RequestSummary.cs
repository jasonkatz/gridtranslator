﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class RequestSummary
    {
        public RequestTypeEnum RequestType { get; set; }
        public string RequestID { get; set; }
        public int Count { get; set; }
    }
    public enum RequestTypeEnum
    {
        Transactions = 0,
        Positions = 1
    }
}
