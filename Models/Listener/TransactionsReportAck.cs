﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class TransactionsReportAck
    {
        public string TradeRequestID { get; set; }
        public TransactionRequestResultType TransactionRequestResultType { get; set; }
        public TransactionRequestStatusType TransactionRequestStatusType { get; set; }
        public List<TransactionsReport> TransactionsReports { get; set; }
    }
}
