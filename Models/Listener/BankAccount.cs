﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class BankAccount
    {
        public string AccountNumber { get; set; }
        public string BranchCode { get; set; }
        public string BankAccountCode { get; set; }
    }
}
