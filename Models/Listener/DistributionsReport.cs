﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class DistributionsReport
    {
        public string DistributionsRequestID { get; set; }
        public string DistributionID { get; set; }
        public string InvestmentCode { get; set; }
        public string Ticker { get; set; }
        public string InvestorCode { get; set; }
        public string BankAccountCode { get; set; }
        public string ReinvestInvestmentCode { get; set; }
        public string ReinvestInstrument { get; set; }
        public bool IncomeReinvested { get; set; }
        public string Currency { get; set; }
        public int PayMethod { get; set; } //wut?
        public string PayMethodDescription { get; set; }
        public decimal TotalUnits { get; set; }
        public string ReinvestInvestorCode { get; set; }
        public DateTime DeclaredDate { get; set; }
        public List<DistributionDetail> DistributionDetails { get; set; }
    }
}
