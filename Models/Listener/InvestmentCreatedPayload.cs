﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class InvestmentCreatedPayload
    {
        public string CorrelationID { get; set; }
        public string InvestmentCode { get; set; }
    }
}
