﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class Investor
    {
        public string InternalID { get; set; }
        public string RegistrationID { get; set; }
        public RegistrationResponseType RegistrationResponseType { get; set; }
        public string RejectReason { get; set; }
        public string InvestorCode { get; set; }
        public List<BankAccount> BankAccounts { get; set; }
        public List<LinkedInvestor> LinkedInvestors { get; set; }
    }
}
