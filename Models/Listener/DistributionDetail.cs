﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class DistributionDetail
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
}
