﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class DistributionsReportAck
    {
        public string DistributionsRequestID { get; set; }
        public DistributionRequestResultType DistributionRequestResultType { get; set; }
        public DistributionRequestStatusType DistributionRequestStatusType { get; set; }
        public List<DistributionsReport> DistributionsReports { get; set; }
    }
}
