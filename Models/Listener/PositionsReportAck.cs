﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class PositionsReportAck
    {
        public string PositionsRequestID { get; set; } //necessary?
        public string MainPositionsRequestID { get; set; } //necessary?
        public PositionRequestResultType PositionRequestResultType { get; set; }
        public PositionRequestStatusType PositionRequestStatusType { get; set; }
        public List<PositionsReport> PositionReports { get; set; }
    }
}