﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Listener
{
    public class InstructionCancelReject
    {
        public string CorrelationID { get; set; }
        public string OriginalCorrelationID { get; set; }
        public string TransactionID { get; set; }
        public string RejectReason { get; set; }
        public DateTime Date { get; set; }
    }
}
