﻿using System;

namespace Models.Listener
{
    public class TransactionsReport
    {
        public string InternalID { get; set; }
        public string TradeRequestID { get; set; }
        public string TradeReportID { get; set; }
        public string CorrelationID { get; set; }
        public string InvestmentCode { get; set; }
        public string Ticker { get; set; }
        public ExecutionResponseType ExecutionResponseType { get; set; }
        public string RejectReason { get; set; }
        public string TransactionDescription { get; set; }
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
        public decimal TradeAmount { get; set; }
        public DateTime ExecuteDate { get; set; }
        public string OrderID { get; set; } //necessary?
        public string OriginalCorrelationID { get; set; } //necessary?
        public string ExecutionID { get; set; } //necessary?
        public InstructionType InstructionType { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal Fee { get; set; }
    }
}
