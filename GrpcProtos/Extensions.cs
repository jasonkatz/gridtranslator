﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fix
{
    public static class Extensions
    {
        public static DateTime ToDateTime(this Date date)
        {
            return new DateTime(date.Year, date.Month, date.Day);
        }
        public static DateTime ToDateTime(this Datetime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Milli);
        }
        public static Datetime ToGrpcDatetime(this DateTime dt)
        {
            return new Datetime
            {
                Year = dt.Year,
                Month = dt.Month,
                Day = dt.Day,
                Hour = dt.Hour,
                Minute = dt.Minute,
                Second = dt.Second,
                Milli = dt.Millisecond
            };
        }
        public static Date ToGrpcDate(this DateTime dt)
        {
            return new Date
            {
                Year = dt.Year,
                Month = dt.Month,
                Day = dt.Day
            };
        }
    }
}
