﻿using Grpc.Net.Client;
using System;
using Fix.Engine;
using System.Threading.Tasks;
using Grpc.Core;
using System.Net.Http;

namespace GridTranslator.Test
{
    class Program
    {
        static async Task Main(string[] args)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            using var channel = GrpcChannel.ForAddress("http://localhost:60001", new GrpcChannelOptions { Credentials = ChannelCredentials.Insecure });
            var client = new FixEngine.FixEngineClient(channel);
            var reply = await client.TestConnectionAsync(new EmptyMsg { });
            Console.WriteLine($"TestConnectionAsync: {reply.Msg}");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
