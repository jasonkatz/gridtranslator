﻿using Fix;
using Fix.Engine;
using Grpc.Core;
using HMACAuthentication.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.Engine;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Utils;

namespace GridTranslator
{
    public class FIXEngineImpl : FixEngine.FixEngineBase
    {
        private IConfiguration _config;
        private readonly ILogger<FIXEngineImpl> _logger;
        private Email _emailClient;
        private List<MailAddress> _toEmails;
        private RestClient _restClient;

        public FIXEngineImpl(IConfiguration config, ILogger<FIXEngineImpl> logger)
        {
            _config = config;
            _logger = logger;
            _emailClient = new Email(config.GetValue<string>("Email:Host"),
                config.GetValue<int>("Email:Port"),
                config.GetValue<string>("Email:Username"),
                config.GetValue<string>("Email:Password"),
                config.GetValue<bool>("Email:SSL"),
                config.GetValue<string>("Email:FromName"),
                config.GetValue<string>("Email:FromEmail"));
            _toEmails = config.GetValue<string>("Email:Escalate").Split(new char[] { ',', ';' }).Where(x => x.Trim() != "").Select(x => new MailAddress(x)).ToList();

            _restClient = new RestClient(_config.GetValue<string>("GridConnect:Endpoint"));
            _restClient.AddDefaultHeaders(new Dictionary<string, string>()
            {
                { "Accept", "application/json" },
                { "Content-Type", "application/json" }
            });

            _restClient.Authenticator = new GridRestSharpHMACAuth(config.GetValue<string>("GridConnect:APIKey"), config.GetValue<string>("GridConnect:SecretKey"));
        }

        public static Individual CastFromGrpcIndividual(IndividualMsg investor, string prevRegID)
        {
            return new Individual
            {
                BirthDate = investor.Birthdate.ToDateTime(),
                DistributionMethodType = Enum.Parse<Models.Engine.DistributionMethodType>(investor.DistributionMethodType.ToString(), true),
                CellNumber = investor.CellNumber,
                HomeNumber = investor.HomeNumber,
                WorkNumber = investor.WorkNumber,
                BankAccounts = investor.BankAccounts.Select(x =>
                {
                    return new BankAccount
                    {
                        AccountHolder = x.AccountHolder,
                        AccountNumber = x.AccountNumber,
                        AccountType = Enum.Parse<Models.Engine.AccountType>(x.AccountType.ToString(), true),
                        BankAccountCode = x.GraphiteBankAccountCode,
                        BankCode = x.GraphiteBankCode,
                        BranchCode = x.BranchCode,
                        BranchName = x.BranchName
                    };
                }).ToList(),
                City = investor.City,
                CountryOfBirth = investor.CountryOfBirth,
                CountryOfIssue = investor.CountryOfIssue,
                CountryOfResidence = investor.CountryOfResidence,
                Email = investor.Email,
                FirstNames = investor.FirstNames,
                Gender = Enum.Parse<Models.Engine.GenderType>(investor.Gender.ToString(), true),
                IdentificationType = Enum.Parse<Models.Engine.IdentificationType>(investor.IdentificationType.ToString(), true),
                IDNumber = investor.IdNumber,
                Initials = investor.Initials,
                InternalID = investor.RegistDtls,
                InvestorCode = investor.GraphiteInvestorCode,
                Language = investor.Language,
                MaritalStatusType = Enum.Parse<Models.Engine.MaritalStatusType>(investor.MaritalStatusType.ToString(), true),
                Nationality = investor.Nationality,
                PassportExpiryDate = investor.PassportExpiryDate.ToDateTime(),
                PassportNumber = investor.PassportNumber,
                PostalCode = investor.PostalCode,
                PreviousRegistrationID = prevRegID,
                Province = investor.Province,
                RegistrationID = "",
                Resident = investor.Resident,
                StreetName = investor.StreetName,
                StreetNumber = investor.StreetNumber,
                Suburb = investor.Suburb,
                Surname = investor.Surname,
                SvsStageID = investor.SvsStageId,
                TaxNumber = investor.TaxNumber,
                TaxOffice = investor.TaxOffice,
                TitleType = Enum.Parse<Models.Engine.TitleType>(investor.TitleType.ToString(), true)
            };
        }

        public static Entity CastFromGrpcCompany(EntityMsg entity, EntityType entityType, string prevRegID)
        {
            return new Entity
            {
                EntityType = entityType,
                DistributionMethodType = Enum.Parse<Models.Engine.DistributionMethodType>(entity.DistributionMethodType.ToString(), true),
                CellNumber = entity.CellNumber,
                HomeNumber = entity.HomeNumber,
                WorkNumber = entity.WorkNumber,
                BankAccounts = entity.BankAccounts.Select(x =>
                {
                    return new BankAccount
                    {
                        AccountHolder = x.AccountHolder,
                        AccountNumber = x.AccountNumber,
                        AccountType = Enum.Parse<Models.Engine.AccountType>(x.AccountType.ToString(), true),
                        BankAccountCode = x.GraphiteBankAccountCode,
                        BankCode = x.GraphiteBankCode,
                        BranchCode = x.BranchCode,
                        BranchName = x.BranchName
                    };
                }).ToList(),
                City = entity.City,
                Email = entity.Email,
                InternalID = entity.RegistDtls,
                InvestorCode = entity.GraphiteInvestorCode,
                PostalCode = entity.PostalCode,
                PreviousRegistrationID = prevRegID,
                Province = entity.Province,
                RegistrationID = "",
                StreetName = entity.StreetName,
                StreetNumber = entity.StreetNumber,
                Suburb = entity.Suburb,
                SvsStageID = entity.SvsStageId,
                TaxNumber = entity.TaxNumber,
                TaxOffice = entity.TaxOffice,
                InceptionDate = entity.InceptionDate.ToDateTime(),
                Name = entity.Name,
                RegistrationNumber = entity.RegistrationNumber,
                VatNumber = entity.VatNumber,
                VatRegistrationDate = entity.VatRegistrationDate.ToDateTime(),
                CountryOfResidence = "710" //HARD CODED TO ZA
            };
        }

        #region registrations
        public override Task<Response> RegisterIndividual(RegisterIndividualMsg request, ServerCallContext context)
        {
            try
            {
                _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
                _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
                _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
                _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
                var indiv = CastFromGrpcIndividual(request.Investor, "");
                RestRequest req = new RestRequest("Investor/RegisterIndividual", Method.POST);
                req.AddParameter("application/json", JsonConvert.SerializeObject(indiv), ParameterType.RequestBody);
                var resp = _restClient.Execute(req);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var individual = JsonConvert.DeserializeObject<Individual>(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = false,
                        Msg = individual.RegistrationID
                    });
                }
                else
                {
                    _logger.LogInformation("baaaaaad");
                    _logger.LogInformation(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = "Exception in GridTranslator, see log for details"
                });
            }
        }
        public override Task<Response> RegisterPrivateCompany(RegisterCompanyMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
            _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
            _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
            var entity = CastFromGrpcCompany(request.Company, EntityType.PRIVATECOMPANY, "");
            entity.LinkedIndividuals = request.Directors.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.DIRECTOR,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList();
            RestRequest req = new RestRequest("Investor/RegisterEntity", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = responseEntity.RegistrationID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> RegisterPublicCompany(RegisterCompanyMsg request, ServerCallContext context)
        {
            try
            {
                _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
                _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
                _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
                _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
                var entity = CastFromGrpcCompany(request.Company, EntityType.PUBLICCOMPANY, "");
                entity.LinkedIndividuals = request.Directors.Select(x =>
                {
                    return new LinkedIndividual
                    {
                        LinkedIndividualType = LinkedIndividualType.DIRECTOR,
                        Individual = CastFromGrpcIndividual(x, "")
                    };
                }).ToList();
                RestRequest req = new RestRequest("Investor/RegisterEntity", Method.POST);
                req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
                var resp = _restClient.Execute(req);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = false,
                        Msg = responseEntity.RegistrationID
                    });
                }
                else
                {
                    return Task.FromResult(new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = "Exception in GridTranslator, see log for details"
                });
            }
        }
        public override Task<Response> RegisterTrust(RegisterTrustMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
            _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
            _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
            var entity = CastFromGrpcCompany(request.Trust, EntityType.TRUST, "");
            entity.LinkedIndividuals = request.Trustees.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.TRUSTEE,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList();
            entity.LinkedIndividuals.AddRange(request.Beneficiaries.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.BENEFICIARY,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList());
            RestRequest req = new RestRequest("Investor/RegisterEntity", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = responseEntity.RegistrationID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion

        #region UpdateStatic
        public override Task<Response> UpdateIndividual(UpdateIndividualMsg request, ServerCallContext context)
        {
            try
            {
                _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
                _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
                _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
                _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
                var indiv = CastFromGrpcIndividual(request.Investor, request.PreviousRegistrationID);
                RestRequest req = new RestRequest("Investor/UpdateIndividual", Method.POST);
                req.AddParameter("application/json", JsonConvert.SerializeObject(indiv), ParameterType.RequestBody);
                var resp = _restClient.Execute(req);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var individual = JsonConvert.DeserializeObject<Individual>(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = false,
                        Msg = individual.RegistrationID
                    });
                }
                else
                {
                    return Task.FromResult(new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = "Exception in GridTranslator, see log for details"
                });
            }
        }

        public override Task<Response> UpdatePrivateCompany(UpdateCompanyMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
            _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
            _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
            var entity = CastFromGrpcCompany(request.Company, EntityType.PRIVATECOMPANY, request.PreviousRegistrationID);
            entity.LinkedIndividuals = request.Directors.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.DIRECTOR,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList();
            RestRequest req = new RestRequest("Investor/UpdateEntity", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = responseEntity.RegistrationID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> UpdatePublicCompany(UpdateCompanyMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
            _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
            _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
            var entity = CastFromGrpcCompany(request.Company, EntityType.PUBLICCOMPANY, request.PreviousRegistrationID);
            entity.LinkedIndividuals = request.Directors.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.DIRECTOR,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList();
            RestRequest req = new RestRequest("Investor/UpdateEntity", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = responseEntity.RegistrationID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> UpdateTrust(UpdateTrustMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            _logger.LogDebug(new string('*', 20) + " PAYLOAD " + new string('*', 20));
            _logger.LogDebug(JsonConvert.SerializeObject(request, Formatting.Indented));
            _logger.LogDebug(new string('*', 20) + " ENDPAYLOAD " + new string('*', 20));
            var entity = CastFromGrpcCompany(request.Trust, EntityType.TRUST, request.PreviousRegistrationID);
            entity.LinkedIndividuals = request.Trustees.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.TRUSTEE,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList();
            entity.LinkedIndividuals.AddRange(request.Beneficiaries.Select(x =>
            {
                return new LinkedIndividual
                {
                    LinkedIndividualType = LinkedIndividualType.BENEFICIARY,
                    Individual = CastFromGrpcIndividual(x, "")
                };
            }).ToList());
            RestRequest req = new RestRequest("Investor/UpdateEntity", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(entity), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseEntity = JsonConvert.DeserializeObject<Entity>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = responseEntity.RegistrationID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #region Orders
        #region purchases
        #region new business
        public override Task<Response> PurchaseNewInvestor(PurchaseNewInvestorMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            try
            {
                var purchase = new Purchase
                {
                    InternalID = "",
                    AdvisorCode = request.AdvisorCode,
                    BankAccountCode = "",
                    CashOrderQuantity = (decimal)request.CashOrderQty,
                    CorrelationID = "",
                    FeeCode = request.GraphiteFeeCode,
                    FeePercentage = request.GraphiteFeeCode,
                    InvestmentCode = "",
                    InvestorCode = "",
                    ProductCode = request.ProductCode,
                    Recurring = null,
                    RegistrationID = request.RegistrationID,
                    SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                    Ticker = request.Ticker,
                    EFT = false
                };
                RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
                req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
                var resp = _restClient.Execute(req);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation("instruction accepted");
                    var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = false,
                        Msg = data.CorrelationID
                    });
                }
                else
                {
                    _logger.LogInformation("instruction NOT accepted");
                    _logger.LogError(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = "Exception in GridTranslator, see log for details"
                });
            }
        }
        public override Task<Response> PurchaseDONewInvestor(PurchaseDONewInvestorMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = "",
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = request.GraphiteFeeCode,
                FeePercentage = request.GraphiteFeeCode,
                InvestmentCode = "",
                InvestorCode = "",
                ProductCode = request.ProductCode,
                Recurring = new RecurringInfo 
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = null
                },
                RegistrationID = request.RegistrationID,
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> PurchaseDONewInvestorEscalate(PurchaseDONewInvestorEscalateMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = "",
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = request.GraphiteFeeCode,
                FeePercentage = request.GraphiteFeeCode,
                InvestmentCode = "",
                InvestorCode = "",
                ProductCode = request.ProductCode,
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = new EscalationInfo
                    {
                        Date = request.EscalateDate.ToDateTime(),
                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.EscalateFrequency.ToString(), true),
                        Percentage = (decimal)request.EscalatePerc
                    }
                },
                RegistrationID = request.RegistrationID,
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #region new account
        public override Task<Response> PurchaseNewAccount(PurchaseNewAccountMsg request, ServerCallContext context)
        {
            try
            {
                _logger.LogInformation(request.SourceOfFundsType.ToString());
                _logger.LogInformation(Models.Engine.SourceOfFundsType.SALARY.ToString());
                _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
                var purchase = new Purchase
                {
                    InternalID = "",
                    AdvisorCode = request.AdvisorCode,
                    BankAccountCode = request.GraphiteBankAccountCode,
                    CashOrderQuantity = (decimal)request.CashOrderQty,
                    CorrelationID = "",
                    FeeCode = request.GraphiteFeeCode,
                    FeePercentage = request.GraphiteFeeCode,
                    InvestmentCode = "",
                    InvestorCode = request.GraphiteInvestorCode,
                    ProductCode = request.ProductCode,
                    Recurring = null,
                    RegistrationID = "",
                    SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                    Ticker = request.Ticker,
                    EFT = false
                };
                RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
                req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
                var resp = _restClient.Execute(req);
                if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation("instruction accepted");
                    var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = false,
                        Msg = data.CorrelationID
                    });
                }
                else
                {
                    _logger.LogInformation("instruction NOT accepted");
                    _logger.LogError(resp.Content);
                    return Task.FromResult(new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = "Exception in GridTranslator, see log for details"
                });
            }
        }
        public override Task<Response> PurchaseDONewAccount(PurchaseDONewAccountMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = request.GraphiteFeeCode,
                FeePercentage = request.GraphiteFeeCode,
                InvestmentCode = "",
                InvestorCode = request.GraphiteInvestorCode,
                ProductCode = request.ProductCode,
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = null
                },
                RegistrationID = "",
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> PurchaseDONewAccountEscalate(PurchaseDONewAccountEscalateMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = request.GraphiteFeeCode,
                FeePercentage = request.GraphiteFeeCode,
                InvestmentCode = "",
                InvestorCode = request.GraphiteInvestorCode,
                ProductCode = request.ProductCode,
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = new EscalationInfo
                    {
                        Date = request.EscalateDate.ToDateTime(),
                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.EscalateFrequency.ToString(), true),
                        Percentage = (decimal)request.EscalatePerc
                    }
                },
                RegistrationID = "",
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #region existing account
        public override Task<Response> PurchaseExistingAccount(PurchaseExistingAccountMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = "",
                FeePercentage = "",
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = "",
                ProductCode = "",
                Recurring = null,
                RegistrationID = "",
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> PurchaseDOExistingAccount(PurchaseDOExistingAccountMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = "",
                FeePercentage = "",
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = "",
                ProductCode = "",
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = null
                },
                RegistrationID = "",
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> PurchaseDOExistingAccountEscalate(PurchaseDOExistingAccountEscalateMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var purchase = new Purchase
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CashOrderQuantity = (decimal)request.CashOrderQty,
                CorrelationID = "",
                FeeCode = "",
                FeePercentage = "",
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = "",
                ProductCode = "",
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = new EscalationInfo
                    {
                        Date = request.EscalateDate.ToDateTime(),
                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.EscalateFrequency.ToString(), true),
                        Percentage = (decimal)request.EscalatePerc
                    }
                },
                RegistrationID = "",
                SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.SourceOfFundsType.ToString(), true),
                Ticker = request.Ticker,
                EFT = false
            };
            RestRequest req = new RestRequest("Instruction/Purchase", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(purchase), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Purchase>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #endregion
        #region redemptions
        public override Task<Response> Redemption(RedemptionMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var redeem = new Redeem
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CorrelationID = "",
                InvestmentCode = request.GraphiteAccount,
                Recurring = null,
                Ticker = request.Ticker,
                Amount = (decimal)request.Amount,
                RedeemType = Enum.Parse<RedeemType>(request.RedemptionType.ToString(), true)
            };
            RestRequest req = new RestRequest("Instruction/Redeem", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(redeem), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Redeem>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> RecurringRedemption(RecurringRedemptionMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var redeem = new Redeem
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CorrelationID = "",
                InvestmentCode = request.GraphiteAccount,
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = null
                },
                Ticker = request.Ticker,
                Amount = (decimal)request.Amount,
                RedeemType = Enum.Parse<RedeemType>(request.RedemptionType.ToString(), true)
            };
            RestRequest req = new RestRequest("Instruction/Redeem", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(redeem), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Redeem>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> RecurringRedemptionEscalate(RecurringRedemptionEscalateMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var redeem = new Redeem
            {
                InternalID = "",
                AdvisorCode = request.AdvisorCode,
                BankAccountCode = request.GraphiteBankAccountCode,
                CorrelationID = "",
                InvestmentCode = request.GraphiteAccount,
                Recurring = new RecurringInfo
                {
                    FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.Frequency.ToString(), true),
                    StartDate = request.StartDate.ToDateTime(),
                    EndDate = request.EndDate.ToDateTime(),
                    PreferredDay = request.PreferredDay,
                    Escalation = new EscalationInfo
                    {
                        Date = request.EscalateDate.ToDateTime(),
                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(request.EscalateFrequency.ToString(), true),
                        Percentage = (decimal)request.EscalatePerc
                    }
                },
                Ticker = request.Ticker,
                Amount = (decimal)request.Amount,
                RedeemType = Enum.Parse<RedeemType>(request.RedemptionType.ToString(), true)
            };
            RestRequest req = new RestRequest("Instruction/Redeem", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(redeem), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<Redeem>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = data.CorrelationID
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #region other
        public override Task<ListOrderResponse> ListOrder(ListOrderMsg request, ServerCallContext context)
        {
            try
            {
                _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
                var isNewBusiness = ! string.IsNullOrEmpty(request.RegistID);
                _logger.LogInformation($"Count = {request.Instructions.Count}");
                var products = request.Instructions.GroupBy(x => $"{x.ProductCode}|{x.GraphiteAccount}");
                if ((products.Count() > 1))
                {
                    return Task.FromResult(new ListOrderResponse
                    {
                        Response = new Response
                        {
                            Error = true,
                            Msg = "Multiple investments or products not allowed on Instruction List."
                        },
                        ListData = request
                    });
                }
                var investmentCode = request.Instructions.First().GraphiteAccount;
                var productCode = request.Instructions.First().ProductCode;
                var count = request.Instructions.Select(x => x.Buy).Distinct().Count();
                _logger.LogInformation($"isNewBusiness = {isNewBusiness}");
                _logger.LogInformation($"count = {count}");
                if (count == 1)
                {
                    _logger.LogInformation("aaaa");
                    if (request.Instructions.First().Buy)
                    {
                        //buy list
                        var list = new PurchaseList
                        {
                            AdvisorCode = request.AdvisorCode,
                            BankAccountCode = request.GraphiteBankAccountCode,
                            InvestmentCode = investmentCode,
                            ProductCode = productCode,
                            InvestorCode = request.GraphiteInvestorCode,
                            ListID = request.ListID,
                            RegistrationID = request.RegistID,
                            FeeCode = request.GraphiteFeeCode,
                            FeePercentage = request.FeePercentage,
                            SourceOfFundsType = Enum.Parse<Models.Engine.SourceOfFundsType>(request.Instructions.First().SourceOfFundsType.ToString(), true),
                            Purchases = request.Instructions.Select(x =>
                            {
                                return new PurchaseBase
                                {
                                    CashOrderQuantity = (decimal)x.Amount,
                                    CorrelationID = "",
                                    InternalID = x.InternalID,
                                    Ticker = x.Ticker,
                                    Recurring = x.RecFrequency == Fix.Engine.FrequencyType.Unknown ? null : new RecurringInfo
                                    {
                                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(x.RecFrequency.ToString(), true),
                                        StartDate = x.RecStartDate.ToDateTime(),
                                        EndDate = x.RecEndDate.ToDateTime(),
                                        PreferredDay = x.RecPreferredDay,
                                        Escalation = x.EscalateFrequency == Fix.Engine.FrequencyType.Unknown ? null : new EscalationInfo
                                        {
                                            Date = x.EscalateDate.ToDateTime(),
                                            FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(x.EscalateFrequency.ToString(), true),
                                            Percentage = (decimal)x.EscalatePerc
                                        }
                                    }
                                };
                            }).ToList()
                        };
                        RestRequest req = new RestRequest("Instruction/ListPurchase", Method.POST);
                        req.AddParameter("application/json", JsonConvert.SerializeObject(list), ParameterType.RequestBody);
                        var resp = _restClient.Execute(req);
                        if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            _logger.LogInformation("instruction accepted");
                            var data = JsonConvert.DeserializeObject<PurchaseList>(resp.Content);
                            foreach (var i in data.Purchases) request.Instructions.First(x => x.InternalID == i.InternalID).ClOrdID = i.CorrelationID;
                            return Task.FromResult(new ListOrderResponse
                            {
                                Response = new Response
                                {
                                    Error = false,
                                    Msg = ""
                                },
                                ListData = request
                            });
                        }
                        else
                        {
                            _logger.LogInformation("instruction NOT accepted");
                            _logger.LogError(resp.Content);
                            return Task.FromResult(new ListOrderResponse
                            {
                                Response = new Response
                                {
                                    Error = true,
                                    Msg = resp.Content
                                },
                                ListData = request
                            });
                        }
                    }
                    else
                    {
                        //sell list
                        var list = new RedeemList
                        {
                            AdvisorCode = request.AdvisorCode,
                            BankAccountCode = request.GraphiteBankAccountCode,
                            InvestmentCode = investmentCode,
                            ListID = request.ListID,
                            RegistrationID = request.RegistID,
                            Redemptions = request.Instructions.Select(x =>
                            {
                                return new RedeemBase
                                {
                                    Amount = (decimal)x.Amount,
                                    RedeemType = Enum.Parse<RedeemType>(x.RedemptionType.ToString(), true),
                                    CorrelationID = "",
                                    InternalID = x.InternalID,
                                    Ticker = x.Ticker,
                                    Recurring = x.RecFrequency == Fix.Engine.FrequencyType.Unknown ? null : new RecurringInfo
                                    {
                                        FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(x.RecFrequency.ToString(), true),
                                        StartDate = x.RecStartDate.ToDateTime(),
                                        EndDate = x.RecEndDate.ToDateTime(),
                                        PreferredDay = x.RecPreferredDay,
                                        Escalation = x.EscalateFrequency == Fix.Engine.FrequencyType.Unknown ? null : new EscalationInfo
                                        {
                                            Date = x.EscalateDate.ToDateTime(),
                                            FrequencyType = Enum.Parse<Models.Engine.FrequencyType>(x.EscalateFrequency.ToString(), true),
                                            Percentage = (decimal)x.EscalatePerc
                                        }
                                    }
                                };
                            }).ToList()
                        };
                        RestRequest req = new RestRequest("Instruction/ListRedeem", Method.POST);
                        req.AddParameter("application/json", JsonConvert.SerializeObject(list), ParameterType.RequestBody);
                        var resp = _restClient.Execute(req);
                        if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            _logger.LogInformation("instruction accepted");
                            var data = JsonConvert.DeserializeObject<RedeemList>(resp.Content);
                            foreach (var i in data.Redemptions) request.Instructions.First(x => x.InternalID == i.InternalID).ClOrdID = i.CorrelationID;
                            return Task.FromResult(new ListOrderResponse
                            {
                                Response = new Response
                                {
                                    Error = false,
                                    Msg = ""
                                },
                                ListData = request
                            });
                        }
                        else
                        {
                            _logger.LogInformation("instruction NOT accepted");
                            _logger.LogError(resp.Content);
                            return Task.FromResult(new ListOrderResponse
                            {
                                Response = new Response
                                {
                                    Error = true,
                                    Msg = resp.Content
                                },
                                ListData = request
                            });
                        }
                    }
                }
                else
                {
                    throw new Exception("ListOrder can not contain a mixture of buy and sell...");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return Task.FromResult(new ListOrderResponse
                {
                    Response = new Response
                    {
                        Error = true,
                        Msg = "Exception"
                    },
                    ListData = request
                });
            }
        }
        public override Task<SwitchResponse> Switch(SwitchMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var sw = new SwitchInstruction
            {
                AdvisorCode = request.AdvisorCode,
                RedeemAmount = (decimal)request.SellAmount,
                RedeemType = Enum.Parse<RedeemType>(request.RedemptionType.ToString(), true),
                CorrelationID = "",
                InvestmentCode = request.GraphiteAccount,
                ListID = request.ListID,
                RedeemInternalID = "",
                RedeemTicker = request.SellTicker,
                Purchases = request.Purchases.Select(x =>
                {
                    return new SwitchPurchase
                    {
                        InternalID = "",
                        CorrelationID = "",
                        Percentage = (decimal)x.Percentage,
                        Ticker = x.Ticker
                    };
                }).ToList()
            };
            RestRequest req = new RestRequest("Instruction/Switch", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(sw), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                _logger.LogInformation("instruction accepted");
                var data = JsonConvert.DeserializeObject<SwitchInstruction>(resp.Content);
                request.SellClOrdID = data.CorrelationID;
                foreach (var b in data.Purchases) request.Purchases.First(x => x.Ticker == b.Ticker).ClOrdID = b.CorrelationID;
                return Task.FromResult(new SwitchResponse
                {
                    Response = new Response
                    {
                        Error = false,
                        Msg = ""
                    },
                    SwitchData = request
                });
            }
            else
            {
                _logger.LogInformation("instruction NOT accepted");
                _logger.LogError(resp.Content);
                return Task.FromResult(new SwitchResponse
                {
                    Response = new Response
                    {
                        Error = true,
                        Msg = resp.Content
                    },
                    SwitchData = request
                });
            }
        }
        public override Task<Response> CancelOrder(CancelOrderMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var cancel = new CancelInstruction
            {
                InstructionType = request.Buy ? Models.InstructionType.BUY : Models.InstructionType.SELL,
                OriginalCorrelationID = request.OriginalClOrdID,
                Ticker = request.Ticker,
                TransactionID = request.TransID
            };
            RestRequest req = new RestRequest("Instruction/Cancel", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(cancel), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = ""
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #endregion
        #region dataRequests
        public override Task<Response> DistributionRequest(DistributionRequestMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var pr = new DistributionsRequest
            {
                AdvisorCode = request.AdvisorCode,
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = request.GraphiteInvestorCode,
                Ticker = request.Ticker,
                FromDate = request.FromDate.ToDateTime(),
                ToDate = request.ToDate.ToDateTime()
            };
            RestRequest req = new RestRequest("Request/Distributions", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(pr), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = ""
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public class PosTransReqResponse
        {
            public string RequestID { get; set; }
        }
        public override Task<Response> PositionsRequest(PositionsRequestMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var pr = new PositionsRequest
            {
                AdvisorCode = request.AdvisorCode,
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = request.GraphiteInvestorCode,
                Ticker = request.Ticker
            };
            RestRequest req = new RestRequest("Request/Positions", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(pr), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var ptrr = JsonConvert.DeserializeObject<PosTransReqResponse>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = ptrr.RequestID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        public override Task<Response> TransactionsRequest(TransactionsRequestMsg request, ServerCallContext context)
        {
            _logger.LogInformation($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            var pr = new TransactionsRequest
            {
                AdvisorCode = request.AdvisorCode,
                InvestmentCode = request.GraphiteAccount,
                InvestorCode = request.GraphiteInvestorCode,
                Ticker = request.Ticker,
                FromDate = request.FromDate.ToDateTime(),
                ToDate = request.ToDate.ToDateTime()
            };
            RestRequest req = new RestRequest("Request/Transactions", Method.POST);
            req.AddParameter("application/json", JsonConvert.SerializeObject(pr), ParameterType.RequestBody);
            var resp = _restClient.Execute(req);
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var ptrr = JsonConvert.DeserializeObject<PosTransReqResponse>(resp.Content);
                return Task.FromResult(new Response
                {
                    Error = false,
                    Msg = ptrr.RequestID
                });
            }
            else
            {
                return Task.FromResult(new Response
                {
                    Error = true,
                    Msg = resp.Content
                });
            }
        }
        #endregion
        #region misc
        public override Task<Response> ForwardMessage(FixMsg request, ServerCallContext context)
        {
            return Task.FromResult(new Response
            {
                Error = true,
                Msg = "Not implemented!"
            });
        }
        public override Task<Response> TestConnection(EmptyMsg request, ServerCallContext context)
        {
            _logger.LogDebug($"Message received on {System.Reflection.MethodBase.GetCurrentMethod().Name} method...");
            return Task.FromResult(new Response
            {
                Error = false,
                Msg = $"Successfully received message at {DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff")} from {context.Peer}"
            });
        }
        #endregion
    }
}
