﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fix.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GridTranslator.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class HealthController : ControllerBase
    {
        private readonly ILogger<HealthController> _logger;
        private readonly FixClient.FixClientClient _fixClient;
        private readonly IConfiguration _config;

        public HealthController(ILogger<HealthController> logger, FixClient.FixClientClient fixClient, IConfiguration config)
        {
            _logger = logger;
            _fixClient = fixClient;
            _config = config;
        }

        [HttpGet]
        public IActionResult Check()
        {
            var ip = HttpContext.Request.Headers.ContainsKey("x-forwarded-for") ? string.Join("|", HttpContext.Request.Headers["x-forwarded-for"]) : HttpContext.Connection.RemoteIpAddress.ToString();
            _logger.LogInformation($"Call received from {ip} on {HttpContext.GetRouteData().Values["controller"]}/{HttpContext.GetRouteData().Values["action"]}");
            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IActionResult TestAuth()
        {
            return Ok(User.Identity.Name);
        }

        [HttpGet]
        public IActionResult TestGRPC()
        {
            try
            {
                _logger.LogInformation($"Attempting gRPC connection on {_config.GetValue<string>("Grpc:ClientPostAddress")}");
                var resp = _fixClient.TestConnection(new EmptyMsg { });
                return Ok(resp);
            }
            catch (Exception x)
            {
                return StatusCode(500, x.Message);
            }
        }
    }
}