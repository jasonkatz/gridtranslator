﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fix.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.Listener;
using Newtonsoft.Json;

namespace GridTranslator.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]/[action]")]
    public class InvestorController : ControllerBase
    {
        private readonly ILogger<InvestorController> _logger;
        private readonly FixClient.FixClientClient _fixClient;
        private readonly IConfiguration _config;

        public InvestorController(ILogger<InvestorController> logger, FixClient.FixClientClient fixClient, IConfiguration config)
        {
            _logger = logger;
            _fixClient = fixClient;
            _config = config;
        }

        [HttpPost]
        public IActionResult RegisterUpdateStatus([FromBody]Investor investor)
        {
            try
            {
                System.IO.File.WriteAllText(System.IO.Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Investor.RegisterUpdateStatus.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), 
                    JsonConvert.SerializeObject(investor, Formatting.Indented));
                var msg = new RegistrationResponseMsg
                {
                    RegistrationID = investor.RegistrationID,
                    RegistDtls = investor.InternalID,
                    GraphiteInvestorCode = investor.InvestorCode,
                    RegistrationResponseType = Enum.Parse<Fix.Client.RegistrationResponseType>(investor.RegistrationResponseType.ToString(), true),
                    RejectReason = investor.RejectReason ?? ""
                };
                foreach (var bank in investor.BankAccounts)
                {
                    msg.BankDetails.Add(new BankDetailMsg
                    {
                        AccountNumber = bank.AccountNumber,
                        BranchCode = bank.BranchCode,
                        GraphiteBankAccountCode = bank.BankAccountCode
                    });
                }
                foreach (var linked in investor.LinkedInvestors)
                {
                    msg.LinkedDetails.Add(new LinkedDetailsMsg
                    {
                        RegistDtls = linked.InternalID,
                        GraphiteInvestorCode = linked.InvestorCode
                    });
                }
                var resp = _fixClient.RegistrationResponse(msg);
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }
    }
}