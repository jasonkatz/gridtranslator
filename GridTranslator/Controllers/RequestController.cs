﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fix;
using Fix.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.Listener;
using Newtonsoft.Json;

namespace GridTranslator.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]/[action]")]
    public class RequestController : ControllerBase
    {
        private readonly ILogger<RequestController> _logger;
        private readonly FixClient.FixClientClient _fixClient;
        private IConfiguration _config;

        public RequestController(ILogger<RequestController> logger, FixClient.FixClientClient fixClient, IConfiguration config)
        {
            _logger = logger;
            _fixClient = fixClient;
            _config = config;
        }

        private void LogPayload<T>(T obj, string filename)
        {
            var guid = Guid.NewGuid().ToString();
            //Directory.CreateDirectory(Path.Combine(_env.WebRootPath, "Payloads"));
            _logger.LogInformation($"Logging payload as {guid}.json");
            System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), filename), JsonConvert.SerializeObject(obj, Formatting.Indented));
        }

        [HttpPost]
        public IActionResult Positions([FromBody]PositionsReportAck positionsReportAck)
        {
            LogPayload(positionsReportAck, $"Request.Positions.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json");
            try
            {
                //var resp = _fixClient.PositionReportAck(new PositionReportAckMsg
                //{
                //    PosReqID = positionsReportAck.PositionsRequestID,
                //    PosMainRptID = positionsReportAck.MainPositionsRequestID,
                //    NumberOfReports = positionsReportAck.PositionReports.Count,
                //    PositionRequestStatusType = Enum.Parse<Fix.Client.PositionRequestStatusType>(positionsReportAck.PositionRequestStatusType.ToString(), true),
                //    PositionRequestResultType = Enum.Parse<Fix.Client.PositionRequestResultType>(positionsReportAck.PositionRequestResultType.ToString(), true)
                //});
                //_logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                var msg = new PositionReportGroupMsg { };
                foreach (var rep in positionsReportAck.PositionReports)
                {
                    msg.PositionReports.Add(new PositionReportMsg
                    {
                        GraphiteAccount = rep.InvestmentCode,
                        Date = rep.Date.ToGrpcDate(),
                        GraphiteInvestorCode = rep.InvestorCode,
                        PosMainRptID = rep.MainPositionsRequestID,
                        PosReqID = rep.PositionsRequestID,
                        Ticker = rep.Ticker,
                        TotalMarketValue = (double)rep.MarketValue,
                        UnitsCertified = (double)rep.UnitsCertified,
                        UnitsCleared = (double)rep.UnitsCleared,
                        UnitsUncleared = (double)rep.UnitsUncleared
                    });
                }
                var resp = _fixClient.PositionReport(msg);
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult Transactions([FromBody]TransactionsReportAck transactionsReportAck)
        {
            LogPayload(transactionsReportAck, $"Request.Transactions.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json");
            try
            {
                //var resp = _fixClient.TransactionReportAck(new TransactionReportAckMsg
                //{
                //    TradeReqID = transactionsReportAck.TradeRequestID,
                //    NumberOfReports = transactionsReportAck.TransactionsReports.Count,
                //    TransactionRequestStatusType = Enum.Parse<Fix.Client.TransactionRequestStatusType>(transactionsReportAck.TransactionRequestStatusType.ToString(), true),
                //    TransactionRequestResultType = Enum.Parse<Fix.Client.TransactionRequestResultType>(transactionsReportAck.TransactionRequestResultType.ToString(), true)
                //});
                //_logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                var msg = new TransactionReportGroupMsg { };
                foreach (var trans in transactionsReportAck.TransactionsReports)
                {
                    msg.TransactionReports.Add(new TransactionReportMsg
                    {
                        GraphiteAccount = trans.InvestmentCode,
                        TradeAmount = (double)trans.TradeAmount,
                        Buy = trans.InstructionType == Models.InstructionType.BUY,
                        ClOrdID = trans.CorrelationID,
                        Datetime = trans.ExecuteDate.ToGrpcDatetime(),
                        ExecID = trans.ExecutionID,
                        ExecutionResponseType = Enum.Parse<Fix.Client.ExecutionResponseType>(trans.ExecutionResponseType.ToString(), true),
                        Fee = (double)trans.Fee,
                        OrderID = trans.OrderID,
                        OriginalClOrdID = trans.OriginalCorrelationID,
                        Price = (double)trans.Price,
                        Quantity = (double)trans.Quantity,
                        RejectReasonTransDesc = trans.ExecutionResponseType == Models.Listener.ExecutionResponseType.REJECTED ? trans.RejectReason : trans.TransactionDescription,
                        Ticker = trans.Ticker,
                        TradeDate = trans.TradeDate.ToGrpcDate(),
                        TradeReportID = trans.TradeReportID,
                        TradeReqID = trans.TradeRequestID
                    });
                }
                var resp = _fixClient.TransactionReport(msg);
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult Distributions([FromBody]DistributionsReportAck distributionsReportAck)
        {
            LogPayload(distributionsReportAck, $"Request.Distributions.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json");
            try
            {
                //var resp = _fixClient.DistributionReportAck(new DistributionReportAckMsg
                //{
                //    DistRequestID = distributionsReportAck.DistributionsRequestID,
                //    DistributionRequestResultType = Enum.Parse<Fix.Client.DistributionRequestResultType>(distributionsReportAck.DistributionRequestResultType.ToString(), true),
                //    DistributionRequestStatusType = Enum.Parse<Fix.Client.DistributionRequestStatusType>(distributionsReportAck.DistributionRequestStatusType.ToString(), true),
                //    NumberOfReports = distributionsReportAck.DistributionsReports.Count
                //});
                //_logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                var msg = new DistributionReportGroupMsg { };
                foreach (var dist in distributionsReportAck.DistributionsReports)
                {
                    var dr = new DistributionReportMsg
                    {
                        Currency = dist.Currency,
                        GraphiteAccount = dist.InvestmentCode,
                        DeclDate = dist.DeclaredDate.ToGrpcDate(),
                        GraphiteBankAccountCode = dist.BankAccountCode,
                        DescPayMethod = dist.PayMethodDescription,
                        PayMethod = dist.PayMethod,
                        DistRequestID = dist.DistributionsRequestID,
                        DistributionID = dist.DistributionID,
                        GraphiteInvestorCode = dist.InvestorCode,
                        IncomeReinvested = dist.IncomeReinvested,
                        ReinvestInstrument = dist.ReinvestInstrument,
                        ReinvestInvestment = dist.ReinvestInvestmentCode,
                        ReinvestInvestor = dist.ReinvestInvestorCode,
                        Ticker = dist.Ticker,
                        TotUnits = (double)dist.TotalUnits
                    };
                    foreach (var dd in dist.DistributionDetails)
                    {
                        dr.DistDetails.Add(new DistDetail
                        {
                            DistName = dd.Name,
                            DistValue = (double)dd.Value
                        });
                    }
                    msg.DistributionReports.Add(dr);
                }
                var resp = _fixClient.DistributionReport(msg);
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult Investments()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var body = reader.ReadToEnd();
                    System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Request.Investments.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), body);
                }
            }
            catch (Exception x)
            {
                _logger.LogError($"Error: {x.Message}");
                _logger.LogError($"StackTrace: {x.StackTrace}");
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult RecurringReports()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var body = reader.ReadToEnd();
                    System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Request.RecurringReports.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), body);
                }
            }
            catch (Exception x)
            {
                _logger.LogError($"Error: {x.Message}");
                _logger.LogError($"StackTrace: {x.StackTrace}");
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult Beneficiaries()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var body = reader.ReadToEnd();
                    System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Request.Beneficiaries.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), body);
                }
            }
            catch (Exception x)
            {
                _logger.LogError($"Error: {x.Message}");
                _logger.LogError($"StackTrace: {x.StackTrace}");
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult LivingAnnuities()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var body = reader.ReadToEnd();
                    System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Request.LivingAnnuities.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), body);
                }
            }
            catch (Exception x)
            {
                _logger.LogError($"Error: {x.Message}");
                _logger.LogError($"StackTrace: {x.StackTrace}");
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult Summary([FromBody]RequestSummary requestSummary)
        {
            try
            {
                LogPayload(requestSummary, $"Request.Summary.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json");
                switch (requestSummary.RequestType)
                {
                    case RequestTypeEnum.Positions:
                        var respPos = _fixClient.PositionReportAck(new PositionReportAckMsg
                        {
                            PosMainRptID = requestSummary.RequestID,
                            PositionRequestResultType = Fix.Client.PositionRequestResultType.Valid,
                            PositionRequestStatusType = Fix.Client.PositionRequestStatusType.Completed,
                            NumberOfReports = requestSummary.Count
                        });
                        _logger.LogInformation(JsonConvert.SerializeObject(respPos, Formatting.Indented));
                        break;
                    case RequestTypeEnum.Transactions:
                        var respTrans = _fixClient.TransactionReportAck(new TransactionReportAckMsg
                        {
                            TradeReqID = requestSummary.RequestID,
                            TransactionRequestResultType = Fix.Client.TransactionRequestResultType.Valid,
                            TransactionRequestStatusType = Fix.Client.TransactionRequestStatusType.Completed,
                            NumberOfReports = requestSummary.Count
                        });
                        _logger.LogInformation(JsonConvert.SerializeObject(respTrans, Formatting.Indented));
                        break;
                }
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }
    }
}