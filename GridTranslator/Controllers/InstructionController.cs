﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Fix;
using Fix.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Models.Listener;
using Newtonsoft.Json;

namespace GridTranslator.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]/[action]")]
    public class InstructionController : ControllerBase
    {
        private readonly ILogger<InstructionController> _logger;
        private readonly FixClient.FixClientClient _fixClient;
        private readonly IConfiguration _config;

        public InstructionController(ILogger<InstructionController> logger, FixClient.FixClientClient fixClient, IConfiguration config)
        {
            _logger = logger;
            _fixClient = fixClient;
            _config = config;
        }

        [HttpPost]
        public IActionResult Execution([FromBody]Execution execution)
        {
            try
            {
                System.IO.File.WriteAllText(System.IO.Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Instruction.Execution.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), JsonConvert.SerializeObject(execution, Formatting.Indented));
                var resp = _fixClient.Execution(new ExecutionMsg
                {
                    GraphiteAccount = execution.InvestmentCode ?? "",
                    TradeAmount = (double)execution.TradeAmount,
                    Buy = execution.InstructionType == Models.InstructionType.BUY,
                    ClOrdID = execution.CorrelationID ?? "",
                    Datetime = execution.ExecuteDate.ToGrpcDatetime(),
                    ExecID = execution.ExecutionID ?? "",
                    ExecutionResponseType = Enum.Parse<Fix.Client.ExecutionResponseType>(execution.ExecutionResponseType.ToString(), true),
                    Fee = (double)execution.Fee,
                    OrderID = execution.OrderID ?? "",
                    OriginalClOrdID = execution.OriginalCorrelationID ?? "",
                    Price = (double)execution.Price,
                    Quantity = (double)execution.Quantity,
                    RejectReasonTransDesc = execution.ExecutionResponseType == Models.Listener.ExecutionResponseType.REJECTED ? execution.RejectReason ?? "" : execution.TransactionDescription ?? "",
                    Ticker = execution.Ticker,
                    TradeDate = execution.TradeDate.ToGrpcDate()
                });
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult CancelReject([FromBody]InstructionCancelReject instructionCancelReject)
        {
            try
            {
                System.IO.File.WriteAllText(System.IO.Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Instruction.CancelReject.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), JsonConvert.SerializeObject(instructionCancelReject, Formatting.Indented));
                var resp = _fixClient.OrderCancelReject(new OrderCancelRejectMsg
                {
                    ClOrdID = instructionCancelReject.CorrelationID,
                    Datetime = instructionCancelReject.Date.ToGrpcDatetime(),
                    OrigClOrdID = instructionCancelReject.OriginalCorrelationID,
                    RejectReason = instructionCancelReject.RejectReason ?? "",
                    TransID = instructionCancelReject.TransactionID
                });
                _logger.LogInformation(JsonConvert.SerializeObject(resp, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult InvestmentCreated(InvestmentCreatedPayload investmentCreated)
        {
            try
            {
                System.IO.File.WriteAllText(System.IO.Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Instruction.InvestmentCreated.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), JsonConvert.SerializeObject(investmentCreated, Formatting.Indented));
                _logger.LogInformation(JsonConvert.SerializeObject(investmentCreated, Formatting.Indented));
                return Ok();
            }
            catch (Exception x)
            {
                string errorID = Guid.NewGuid().ToString();
                _logger.LogError($"Error {errorID}: {x.Message}");
                _logger.LogError($"StackTrace {errorID}: {x.StackTrace}");
                return StatusCode(500, $"Internal error. Error ID: {errorID}. Please contact support.");
            }
        }

        [HttpPost]
        public IActionResult Status()
        {
            try
            {
                using (var reader = new StreamReader(Request.Body))
                {
                    var body = reader.ReadToEnd();
                    System.IO.File.WriteAllText(Path.Combine(_config.GetValue<string>("LogPayloadPath"), $"Instruction.Status.{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.json"), body);
                }
            }
            catch (Exception x)
            {
                _logger.LogError($"Error: {x.Message}");
                _logger.LogError($"StackTrace: {x.StackTrace}");
            }
            return Ok();
        }
    }
}