using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fix.Client;
using Grpc.Core;
using HMACAuthentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GridTranslator
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public ILogger<Startup> _logger { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication("HMAC-SHA256").AddHMACAuthentication();
            services.AddControllers();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddTransient<FixClient.FixClientClient>(x => new FixClient.FixClientClient(new Channel(Configuration.GetValue<string>("Grpc:ClientPostAddress"), ChannelCredentials.Insecure)));
            services.AddSingleton<FIXEngineImpl>();
            services.AddSingleton<MockUserStore>();
            services.AddSingleton<IRolesLookup>(x => x.GetRequiredService<MockUserStore>());
            services.AddSingleton<ISecretLookup>(x => x.GetRequiredService<MockUserStore>());
        }

        public class MockUserStore : IRolesLookup, ISecretLookup
        {
            private IConfiguration _config;

            public MockUserStore(IConfiguration config)
            {
                _config = config;
            }

            public async Task<string[]> LookupRolesAsync(string apiKey)
            {
                if (apiKey == _config.GetValue<string>("API:APIKey"))
                {
                    return new string[] { "User" };
                }
                return null;
            }

            public async Task<string> LookupSecretAsync(string apiKey)
            {
                if (apiKey == _config.GetValue<string>("API:APIKey"))
                {
                    return _config.GetValue<string>("API:SecretKey");
                }
                return null;
            }

            public async Task<byte[]> LookupSecretBytesAsync(string apiKey)
            {
                return Encoding.UTF8.GetBytes(await LookupSecretAsync(apiKey));
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;
            var grpcServer = new Server
            {
                Services = { Fix.Engine.FixEngine.BindService(app.ApplicationServices.GetRequiredService<FIXEngineImpl>()) },
                Ports = { new ServerPort("0.0.0.0", Configuration.GetValue<int>("Grpc:BindPort"), ServerCredentials.Insecure) }
            };
            grpcServer.Start();
            _logger.LogInformation($"grpc server started on bound port {Configuration.GetValue<int>("Grpc:BindPort")}...");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
