﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMACAuthentication.Client
{
    public class GridRestSharpHMACAuth : IAuthenticator
    {
        private string _apiKey;
        private string _secretKey;

        public GridRestSharpHMACAuth(string apiKey, string secretKey)
        {
            _apiKey = apiKey;
            _secretKey = secretKey;
        }

        public void Authenticate(IRestClient client, IRestRequest request)
        {
            var nonce = SignatureHelper.GenerateNonce(12);
            var unix = (long)DateTime.UtcNow.Subtract(DateTime.UnixEpoch).TotalSeconds;
            var body = "multipart/form-data";
            var cth = request.Parameters.FirstOrDefault(x => x.Type == ParameterType.HttpHeader && x.Name == "Content-Type");
            if (cth == null || !cth.Value.ToString().ToLower().Contains("multipart/form-data"))
            {
                body = request.Body?.Value?.ToString() ?? "";
            }

            var uri = Uri.EscapeUriString(client.BuildUri(request).ToString()).Replace("https://", "").Replace("http://", "").ToLower();
            string authenticationSignature = SignatureHelper.GenerateSignature(_secretKey,
                request.Method.ToString().ToUpper(),
                uri,
                body,
                _apiKey,
                nonce,
                unix);
            request.AddHeader("Authorization",
                $"HMAC-SHA256 {SignatureHelper.GenerateAuthorizationHeaderString(_apiKey, nonce, authenticationSignature, unix)}");
        }
    }
}