﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace HMACAuthentication.Client
{
    public static class SignatureHelper
    {
        public static string GenerateSignature(string secret, string method, string resource, string content, string apiKey, string nonce, long unixTS)
        {
            string md5Content = "";
            using (var md5 = MD5.Create())
            {
                md5Content = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(content ?? "")));
            }
            var signatureContent = $"{method}\n{resource}\n{md5Content}\n{apiKey}\n{nonce}\n{unixTS}";
            using (var hmac = new HMACSHA256())
            {
                hmac.Key = Encoding.UTF8.GetBytes(secret);
                return Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(signatureContent)));
            }
        }
        public static string GenerateAuthorizationHeaderString(string apiKey, string nonce, string signature, long unixTS)
        {
            return $"{apiKey}:{nonce}:{signature}:{unixTS}";
        }
        public static string GenerateNonce(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmonpqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}