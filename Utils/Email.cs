﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Utils
{
    public class Email
    {
        private SmtpClient _client { get; set; }
        private string _fromName { get; set; }
        private string _fromEmail { get; set; }

        public Email(string hostname, int port, string username, string password, bool ssl, string fromName, string fromEmail)
        {
            _fromName = fromName;
            _fromEmail = fromEmail;
            _client = new SmtpClient(hostname, port);
            _client.Credentials = new System.Net.NetworkCredential(username, password);
            _client.EnableSsl = ssl;
            _client.Timeout = 90000;
        }

        public void Send(List<MailAddress> to, List<MailAddress> cc, List<MailAddress> bcc, string subject, string body)
        {
            MailMessage msg = new MailMessage();
            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;
            msg.From = new MailAddress(_fromEmail, _fromName);
            foreach (MailAddress add in to) msg.To.Add(add);
            foreach (MailAddress add in cc) msg.CC.Add(add);
            foreach (MailAddress add in bcc) msg.Bcc.Add(add);

            _client.Send(msg);
        }
        public void Send(List<MailAddress> to, string subject, string body) { Send(to, new List<MailAddress>(), new List<MailAddress>(), subject, body); }
        public void Send(List<MailAddress> to, List<MailAddress> cc, string subject, string body) { Send(to, cc, new List<MailAddress>(), subject, body); }
    }
}
